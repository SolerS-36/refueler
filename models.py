from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from datetime import datetime


db = SQLAlchemy()  # TODO app?


class CarModel(db.Model):
    __tablename__ = 'car'

    id = db.Column(UUID(), primary_key=True)
    license_plate = db.Column(db.String())
    orders = db.relationship("OrderModel", backref="cars")

    def __init__(self, license_plate):
        self.id = str(uuid4())
        self.license_plate = license_plate


class OrderModel(db.Model):
    __tablename__ = 'order'

    id = db.Column(UUID(), primary_key=True)
    car_id = db.Column(UUID(), db.ForeignKey('car.id'),  nullable=False)
    status = db.Column(db.Enum("QUEUED", "PROCESSING",
                       "FINISHED", "CANCELLED", name='status'))
    car_action = db.Column(db.Enum("LOCK", "UNLOCK", name='car_action'))
    lock_word = db.Column(db.String, nullable=True)
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    fuel_level = db.Column(db.Float)
    order_histories = db.relationship("OrderHistoryModel", backref="orders")

    def __init__(self, car_id, status, lat, lon, fuel_level, car_action="LOCK", lock_word=None):
        self.id = str(uuid4())
        self.car_id = car_id
        self.status = status
        self.car_action = car_action
        self.lock_word = lock_word
        self.lat = lat
        self.lon = lon
        self.fuel_level = fuel_level

    def json(self):
        return {
            "status": self.status,
            "car_action": self.car_action,
            "id": self.id,
            "car_id": self.car_id,
            "lat": self.lat,
            "lon": self.lon,
            "fuel_level": self.fuel_level
        }


class OrderHistoryModel(db.Model):
    __tablename__ = 'order_history'

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(UUID(), db.ForeignKey('order.id'))
    new_order_state = db.Column(db.JSON, nullable=True)
    time = db.Column(db.DateTime(timezone=True))
    initiator = db.Column(db.Enum("WEB", "CARSHARING", "REFUELER", name='initiator'))
    result = db.Column(db.String())
    value_to_assign = db.Column(db.Enum("QUEUED", "PROCESSING","FINISHED", 
                                        "CANCELLED", "LOCK", "UNLOCK", name='value_to_assign'))

    def __init__(self, order_id, new_order_state, initiator, result, value_to_assign):
        self.order_id = order_id
        self.new_order_state = new_order_state
        self.time = datetime.now()
        self.initiator = initiator
        self.result = result
        self.value_to_assign = value_to_assign

    def json(self):
        return {
            "id": self.id,
            "order_id": self.order_id,
            "new_order_state": self.new_order_state,
            "time": self.time.strftime("%d-%m-%Y  %H:%M:%S.%f"),
            "initiator": self.initiator,
            "result": self.result,
            "value_to_assign": self.value_to_assign
        }
