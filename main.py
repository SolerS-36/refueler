from flask import Flask, request, render_template, url_for, redirect
from flask_restful import Api
from flask_migrate import Migrate
from models import db
import integration.order_service as OrderService
import integration.car_cervices as CarService
from integration.api import OrdersDelApi, OrdersPostApi, CarPostApi
from integration.exception import RefuelerException, CarNotFound, ValidationError


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://refueler:refueler@localhost/refueler"
app.config['SECRET_KEY'] = '198be83a468aeaaa4320cb8274fd9e75c137f12f06e1469d79115f1ce0e3dda0'
api_key = 'Вставьте сюда ваш API Key от яндекс карт'
path_to_geojson = "/static/json/geo.json"

api = Api(app)
api.add_resource(OrdersPostApi, '/orders')
api.add_resource(OrdersDelApi, '/orders/<id>')
api.add_resource(CarPostApi, '/cars')


migrate = Migrate(app, db)

db.init_app(app)


@app.before_first_request
def create_table():
    db.create_all()


@app.route("/")
def index():
    orders_list = OrderService.get_all_orders()
    return render_template('index.html', orders=orders_list, title='Refueler')


@app.route("/map")
def map():
    orders_list = OrderService.get_active_orders_for_cars()
    return render_template('map.html', orders=orders_list, api_key=api_key, path_to_geojson=path_to_geojson, title='Карта')


@app.route('/web/car/<UUID_or_license_plate>')
def orders_for_car(UUID_or_license_plate: str):
    try:
        orders = CarService.get_ordders_for_car(UUID_or_license_plate)
        return render_template('orders_for_car.html', context=orders,
                               id=UUID_or_license_plate, msg='')
    except CarNotFound as err:
        return render_template('orders_for_car.html', context=[],
                               id=UUID_or_license_plate, msg=repr(err))
    except ValidationError as err:
        return render_template('orders_for_car.html', context=[],
                               id=UUID_or_license_plate, msg=repr(err))


@app.route("/web/history_by_order/<id>")
def history_by_order(id: str):
    msg = request.args.get('msg')
    history = OrderService.find_history_entries_by_order(id)
    # msg и id передать внутри сессии
    return render_template('history_by_order_id.html', context=history, id=id, context_msg=msg, title='Refueler')


@app.route("/web/orders/<id>/action/<action>", methods=["POST"])
def execute_action(id: str, action: OrderService.any_car_action):
    try:
        OrderService.execute_car_action(id, action, "WEB")
        return redirect(url_for('history_by_order', id=id, msg=''))
    except RefuelerException as err:
        return redirect(url_for('history_by_order', id=id, msg=repr(err)))


@app.route("/web/orders/<id>/status/<status>", methods=["POST"])
def change_status(id: str, status: OrderService.any_status):
    try:
        OrderService.change_status(id, status, "WEB")
        return redirect(url_for('history_by_order', id=id, msg=''))
    except RefuelerException as err:
        return redirect(url_for('history_by_order', id=id, msg=repr(err)))


app.debug = True
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
