import re
from typing import List, Literal
import integration.database_client as DatabaseClient
from integration.exception import ValidationError
from models import CarModel


def validation_license_plate(license_plate: str) -> Literal[True]:
    re_license_plate = re.compile(
        r'[АВЕКМНОРСТУХ]\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\d{2,3}', re.I)
    if re.fullmatch(re_license_plate, license_plate):
        return True
    raise ValidationError("ГРЗ введен неверно.")

def validation_uuid(uuid:str) -> Literal[True]:
    re_uuid = re.compile(
        r'[0-9a-f]{8}(?:-[0-9a-f]{4}){4}[0-9a-f]{8}', re.I)
    if re.search(re_uuid, uuid):
        return True
    raise ValidationError("UUID введен неверно.")

def create_car(license_plate: str) -> CarModel:
    if validation_license_plate(license_plate):
        return DatabaseClient.create_car(license_plate)

def find_car(license_plate: str) -> CarModel:
    if validation_license_plate(license_plate):
        return DatabaseClient.find_car(license_plate)

def get_ordders_for_car(uuid_or_license_plate: str) -> List[DatabaseClient.OrderContainer]:
    if len(uuid_or_license_plate) < 15:
        if validation_license_plate(uuid_or_license_plate):
            return DatabaseClient.find_orders_for_car_by_license_plate(uuid_or_license_plate)
    else:
        if validation_uuid(uuid_or_license_plate):
            return DatabaseClient.find_orders_for_car_by_id(uuid_or_license_plate)
