from dataclasses import dataclass
from typing import Any, Dict, Mapping, Tuple
from flask_restful import Resource
from flask import request
import integration.order_service as OrderService
import integration.car_cervices as CarService
from integration.exception import CarAlreadyHasActiveOrder, CarCreateException

#Эти вызовы приходят от CarsharingCar 
#это нужно воспринимать, как обращение кастомера к вендору YouDrive


class OrdersPostApi(Resource):
    def post(self) -> Tuple[Dict[str,str], int]:
        """Принимает данные о машине и на их основе создает новый ордер."""
        try:
            data: Mapping[str, Any] = request.get_json()
            if data.keys() != {'carId', 'lat', 'lon', 'intervalStart', 'intervalEnd', 'confirmed', 'fuelLevel'}:
                return {'orderId':"NULL", 'success': "false"},400
            new_order = OrderService.create_order(data['carId'], data["lat"], data["lon"], data["fuelLevel"])
            OrderService.create_order_history_entry(new_order.id, new_order.json(), initiator="CARSHARING", result='201', value_to_assign="QUEUED")
            return {'orderId':new_order.id, 'success': "true"},201
        except CarAlreadyHasActiveOrder as err:
            print(err)
            return {'success': "false", 'error': repr(err)},409
        except Exception as err:
            print(err)
            return {'success': "false", 'error': repr(err)},500


class OrdersDelApi(Resource):
    def delete(self, id) -> Tuple[Dict[str,str], int]:
        """Изменяет статус ордера на CANCELLED."""
        try:
            order = OrderService.find_order(id)
            if order:
                if order.status == "QUEUED":
                    OrderService.cancel_order(id)
                    OrderService.create_order_history_entry(order.id, order.json(), initiator="CARSHARING", result='200', value_to_assign="CANCELLED")
                    return {'success': "true"}, 200
                else:
                    OrderService.create_order_history_entry(order.id, new_order_state=None, initiator="CARSHARING", result='406', value_to_assign="CANCELLED")
                    return {'success': "false"}, 406
            else:
                return {'success': "false"}, 404
        except Exception as err:
            print(err)
            return {'success': "false", 'error': repr(err)},500


class CarPostApi(Resource):
    def post(self) -> Tuple[Dict[str,str], int]:
        """Проверяет существует ли этот автомобиль в базе данных, если нет добавляет его."""
        try:
            data: Mapping[str, Any] = request.get_json()
            if data.keys() != {'model', 'color', 'number', 'fuelTypeId'}:
                return {'carId':"NULL", 'success': "false"},400
            car = CarService.find_car(data['number'])
            if not car:
                car = CarService.create_car(data['number'])
                return {'carId':car.id, 'success': "true"},201
            else:
                raise CarCreateException("В базе данных уже существует автомобиль с таким ГРЗ.")
        except Exception as err:
            print(err)
            return {'success': "false", 'error': repr(err)},500

