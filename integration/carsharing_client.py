from typing import Any, Mapping
import requests
from integration.database_client import any_status, any_car_action
from integration.exception import CarsharingClientCodeException, CarsharingClientKeyError
from models import OrderModel


url_carsharing_api = "https://carsharing.free.beeceptor.com"

text_exeption = "{code} : Refill {id} already {0}.".format
text_exeption_queued = "{code} : Refill {id} not in PROCESSING state."
change_status_error_mapping: Mapping[int, Any] = {
        406: {"QUEUED": {"FINISHED": text_exeption_queued, "CANCELLED": text_exeption_queued}, 
            "FINISHED": {"QUEUED": text_exeption("SUCCEED", id='{id}', code='{code}'), "PROCESSING": text_exeption("FAILED", id='{id}', code='{code}'),
                        "FINISHED": "{code} :Other already succeeded with result.", "CANCELLED": text_exeption("SUCCEED", id='{id}', code='{code}')}, 
            "CANCELLED": {"QUEUED": text_exeption("CANCELLED", id='{id}', code='{code}'), "PROCESSING": text_exeption("CANCELLED", id='{id}', code='{code}'), 
                        "FINISHED": text_exeption("CANCELLED", id='{id}', code='{code}'), "CANCELLED": text_exeption("CANCELLED", id='{id}', code='{code}')}},
        400: {"PROCESSING": {"FINISHED": "{code} : One (several) of the fields is not filled in: fuel_amount, fuel_price,delivery_price, total_order_price", 
                            "CANCELLED": "{code} : The field is not filled in 'cancellation_reason'."}},
        404: "404 : Прислали id несуществующего ордера.", 
        403: "403 : CarActionForbidden", 
        412: "412 : CarActionPreconditionFailed", 
        500: "500 : InternalServerError"}



text_exeption_1 = "406 : Vendor can't do anything with the car in not processing state!"
dict_car_action = {"LOCK": text_exeption_1, "UNLOCK": text_exeption_1}
execute_car_action_error_mapping = {"QUEUED": dict_car_action, "FINISHED": dict_car_action, "CANCELLED": dict_car_action}


def status_change(order:OrderModel, new_status: any_status) -> None:

    try:
        url = "{url_carsharing_api}/api/v{version}/refueler-pump-order/{orderId} ".format(url_carsharing_api=url_carsharing_api, version="1", orderId=order.id)
        response = requests.post(url, json={'status':new_status})  
        response_code = response.status_code
        if response_code == 200:
            return 
        elif response_code == 400 or response_code == 406:
            raise CarsharingClientCodeException(change_status_error_mapping[response_code][order.status][new_status].format(id=order.id, code=response_code)) 
        else:
            raise CarsharingClientCodeException(change_status_error_mapping[response_code])
    except KeyError:
        raise CarsharingClientKeyError("Нарушена логика при выдаче кода ответа.") 


def execute_car_action(order_id: str, car_status: any_status, car_action: any_car_action) -> None:
    try:
        url = "{url_carsharing_api}/api/v{version}/refueler-pump-car".format(url_carsharing_api=url_carsharing_api, version="1")
        response = requests.put(url, json={'action':car_action, 'orderId': order_id})
        response_code = response.status_code
        if response_code == 200:
            return 
        elif response_code == 406:
            raise CarsharingClientCodeException(execute_car_action_error_mapping[car_status][car_action])
        else:
            raise CarsharingClientCodeException(change_status_error_mapping[response_code])
    except KeyError:
        raise CarsharingClientKeyError("Нарушена логика при выдаче кода ответа.")



