from datetime import datetime
from typing import Callable, Dict, List, Literal, Union
from sqlalchemy import desc, text
from models import db, CarModel, OrderModel, OrderHistoryModel
from integration.exception import LockOrderException, CarNotFound
from uuid import uuid4


any_status  =  Literal['QUEUED', 'PROCESSING', 'FINISHED', 'CANCELLED']
any_car_action = Literal["LOCK", "UNLOCK"]
any_initiator = Literal["WEB", "CARSHARING", "REFUELER"]


class OrderContainer():
    def __init__(self, license_plate:str, order_id:str, status: any_status, car_action: any_car_action, time:datetime, lat:float, lon:float, fuel_level:float):
        self.license_plate = license_plate
        self.id = order_id
        self.status = status
        self. car_action = car_action
        self.time = time
        self.lat = lat
        self.lon = lon
        self.fuel_level = fuel_level


def create_order(car_id:str, lat:float, lon:float, fuel_level:float) -> OrderModel:
    order = OrderModel(car_id,status="QUEUED", lat=lat, lon=lon, fuel_level=fuel_level)
    db.session.add(order)
    db.session.commit()
    return order


def get_all_orders() -> List[OrderContainer]:
    orders = db.session.query(CarModel, OrderModel, OrderHistoryModel
                        ).select_from(CarModel
                        ).outerjoin(OrderModel
                        ).outerjoin(OrderHistoryModel
                        ).group_by(CarModel.license_plate, OrderModel.id
                        ).values(
                            CarModel.license_plate,
                            OrderModel.id,
                            OrderModel.status,
                            OrderModel.car_action,
                            db.func.max(OrderHistoryModel.time),
                            OrderModel.lat,
                            OrderModel.lon,
                            OrderModel.fuel_level)
    orders_list = []
    for order in orders:
        orders_list.append(OrderContainer(*order))
    return orders_list
    

def get_active_orders_for_cars() -> List[OrderContainer]:
    orders = db.session.query(CarModel, OrderModel, OrderHistoryModel
                        ).select_from(CarModel
                        ).outerjoin(OrderModel
                        ).outerjoin(OrderHistoryModel
                        ).group_by(CarModel.license_plate, OrderModel.id
                        ).filter((OrderModel.status=="QUEUED") | (OrderModel.status=="PROCESSING")
                        ).values(
                            CarModel.license_plate,
                            OrderModel.id,
                            OrderModel.status,
                            OrderModel.car_action,
                            db.func.max(OrderHistoryModel.time),
                            OrderModel.lat,
                            OrderModel.lon,
                            OrderModel.fuel_level)
    orders_list = []
    for order in orders:
        orders_list.append(OrderContainer(*order))
    return orders_list


def find_order(order_id:str) -> OrderModel:
    return OrderModel.query.filter_by(id=order_id).first()


def do_with_lock(func:Callable, order_id:str, operation: Union[any_status, any_car_action], initiator: any_initiator=None) -> None:
    try:
        lock_word = try_lock_order(order_id)
        func(order_id, operation, initiator)
    finally:
        if 'lock_word' in vars():
            unlock_order(order_id, lock_word)


def try_lock_order(order_id:str) -> str:
    lock_word = str(uuid4())
    sql = """UPDATE "order" SET lock_word = :lock_word WHERE id = :id AND lock_word is null;"""
    result = db.engine.execute(text(sql).execution_options(autocommit=True), id = order_id, lock_word = lock_word)
    if result.rowcount != 1:
        raise LockOrderException("Эту строку сейчас нельзя изменять. Попробуйте позже.")
    return lock_word


def unlock_order(order_id:str, lock_word:str) -> None:
    sql: str = """UPDATE "order" SET lock_word = null WHERE id = :id and lock_word = :lock_word;"""
    db.engine.execute(text(sql).execution_options(autocommit=True), id = order_id, lock_word = lock_word)


def update_order(order: OrderModel) -> None:
    db.session.add(order)
    db.session.commit()


def create_order_history_record(order_id: str, new_order_state: Union[Dict[str, Union[str, float]], None], initiator: any_initiator, result: str, value_to_assign: Union[any_status, any_car_action]) -> None:
    record_in_history = OrderHistoryModel(order_id, new_order_state=new_order_state, initiator=initiator, result=result, value_to_assign=value_to_assign)
    db.session.add(record_in_history)
    db.session.commit()


def find_history_entries_by_order(order_id:str) -> List[OrderHistoryModel]:
    return OrderHistoryModel.query.filter_by(order_id=order_id).order_by(desc(OrderHistoryModel.time))


def create_car(license_plate:str) -> CarModel:
    car = CarModel(license_plate)
    db.session.add(car)
    db.session.commit()
    return car


def find_car(license_plate) -> CarModel:
    return CarModel.query.filter_by(license_plate=license_plate).first()


def find_orders_for_car_by_license_plate(license_plate) -> List[OrderContainer]:
    orders = db.session.query(CarModel, OrderModel, OrderHistoryModel
                        ).select_from(CarModel
                        ).outerjoin(OrderModel
                        ).outerjoin(OrderHistoryModel
                        ).filter(CarModel.license_plate==license_plate
                        ).group_by(OrderModel.id, CarModel.license_plate
                        ).values(  
                            CarModel.license_plate,                                                                                  
                            OrderModel.id,
                            OrderModel.status,
                            OrderModel.car_action,
                            db.func.max(OrderHistoryModel.time),
                            OrderModel.lat,
                            OrderModel.lon,
                            OrderModel.fuel_level)
    orders_list_for_car = []
    for order in orders:
        orders_list_for_car.append(OrderContainer(*order))
    if len(orders_list_for_car) < 1:
        raise CarNotFound("Автомобиль с таким license_plate не найден.")
    return orders_list_for_car


def find_orders_for_car_by_id(car_id) -> List[OrderContainer]:
    orders = db.session.query(CarModel, OrderModel, OrderHistoryModel
                        ).select_from(CarModel
                        ).outerjoin(OrderModel
                        ).outerjoin(OrderHistoryModel
                        ).filter(OrderModel.car_id==car_id
                        ).group_by(OrderModel.id, CarModel.license_plate
                        ).values(  
                            CarModel.license_plate,                                                                                  
                            OrderModel.id,
                            OrderModel.status,
                            OrderModel.car_action,
                            db.func.max(OrderHistoryModel.time),
                            OrderModel.lat,
                            OrderModel.lon,
                            OrderModel.fuel_level)
    orders_list_for_car = []
    for order in orders:
        orders_list_for_car.append(OrderContainer(*order))
    if len(orders_list_for_car) < 1:
        raise CarNotFound("Автомобиль с таким car_id не найден.")
    return orders_list_for_car


def find_active_orders_for_car(car_id) -> List[OrderModel]:
    return OrderModel.query.filter(OrderModel.car_id==car_id).filter((OrderModel.status=="QUEUED") | (OrderModel.status=="PROCESSING")).all()

