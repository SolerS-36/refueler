
class RefuelerException(Exception):
    pass


class OrderNotFound(RefuelerException):
    pass


class CarNotFound(RefuelerException):
    pass


class CarsharingClientCodeException(RefuelerException):
    pass


class CarsharingClientKeyError(RefuelerException):
    pass


class LockOrderException(RefuelerException):
    pass


class CarAlreadyHasActiveOrder(RefuelerException):
    pass


class CarCreateException(RefuelerException):
    pass


class ValidationError(RefuelerException):
    pass


