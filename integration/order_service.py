import json
from typing import Dict, Literal, Union
from shapely.geometry import shape, Point
import integration.car_cervices as CarService
import integration.database_client as DatabaseClient
from integration.database_client import any_status, any_car_action, any_initiator 
import integration.carsharing_client as CarsharingClient
from integration.exception import OrderNotFound, CarsharingClientCodeException, CarAlreadyHasActiveOrder, CarCreateException


def create_order(car_id: str, lat: float, lon: float, fuel_level: float):
    if CarService.validation_uuid(car_id):
        if check_coordinates_for_entry(float(lat), float(lon)):
            if len(DatabaseClient.find_active_orders_for_car(car_id)) > 0:
                raise CarAlreadyHasActiveOrder(
                    "Автомобиль c car_id = {0}, имеет ордер с активным статусом.".format(car_id))
            return DatabaseClient.create_order(car_id, lat, lon, fuel_level)
    


def change_status(order_id: str, status: any_status, initiator: any_initiator) -> None:
    def anonymous(order_id: str, status: any_status, initiator: any_initiator) -> None:
        try:
            order = DatabaseClient.find_order(order_id)
            CarsharingClient.status_change(order, status)
            if order:
                order.status = status
                DatabaseClient.update_order(order)
                DatabaseClient.create_order_history_record(
                    order.id, order.json(), initiator, result='OK', value_to_assign=status)
            else:
                raise OrderNotFound("Ордер не найденю")
        except CarsharingClientCodeException as err:
            DatabaseClient.create_order_history_record(
                order.id, order.json(), initiator, result=str(err), value_to_assign=status)
    if CarService.validation_uuid(order_id):
        DatabaseClient.do_with_lock(anonymous, order_id, status, initiator)


def cancel_order(order_id: str) ->  None:
    def anonymous(order_id: str, status: any_status, initiator: any_initiator=None) -> None:
        order = DatabaseClient.find_order(order_id)
        order.status = status
        DatabaseClient.update_order(order)
    if CarService.validation_uuid(order_id):
        DatabaseClient.do_with_lock(anonymous, order_id, "CANCELLED")


def execute_car_action(order_id: str, car_action: any_car_action, initiator: any_initiator) -> None:
    def anonymous(order_id: str, car_action: any_car_action, initiator: any_initiator):
        try:
            order = DatabaseClient.find_order(order_id)
            CarsharingClient.execute_car_action(order_id, order.status, car_action)
            if order:
                order.car_action = car_action
                DatabaseClient.update_order(order)
                DatabaseClient.create_order_history_record(
                    order.id, order.json(), initiator, result='OK', value_to_assign=car_action)
            else:
                raise OrderNotFound("Ордер не найден.")
        except CarsharingClientCodeException as err:
            DatabaseClient.create_order_history_record(
                order.id, order.json(), initiator, result=str(err), value_to_assign=car_action)
    if CarService.validation_uuid(order_id):
        DatabaseClient.do_with_lock(anonymous, order_id, car_action, initiator)


def create_order_history_entry(order_id: str, new_order_state: Union[Dict[str, Union[str, float]], None], initiator: any_initiator, result: str, value_to_assign: Union[any_status, any_car_action]) -> None:
    DatabaseClient.create_order_history_record(
        order_id, new_order_state, initiator, result, value_to_assign)


def find_history_entries_by_order(order_id:str):
    if CarService.validation_uuid(order_id):
        return DatabaseClient.find_history_entries_by_order(order_id)


def find_order(id: str):
    if CarService.validation_uuid(id):
        return DatabaseClient.find_order(id)


def get_all_orders():
    return DatabaseClient.get_all_orders()


def get_active_orders_for_cars():
    return DatabaseClient.get_active_orders_for_cars()


def check_coordinates_for_entry(lat: float, lon: float) -> Literal[True]:
    json_file = 'static/json/geo.json'
    with open(json_file) as f:
        js = json.load(f)
    point = Point(lon, lat)
    for feature in js['features']:
        polygon = shape(feature['geometry'])
        if polygon.contains(point):
            return True
        else:
            raise CarCreateException('Автомобиль находится вне зоны заправки.')
    raise CarCreateException("В файле {0} не найдены полигоны.".format(json_file))
