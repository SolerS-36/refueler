Инстукция по установке:

- python3 --version проверить установлен ли  Python3 тестировалось на версии 3.8.* и 3.10.4.

- pip3 --version     проверить установлен ли pip

- pip3 install --user --upgrade pip   обновление pip 

- python3 -m venv refueler   создание виртуального окружения   https://digitology.tech/docs/python_3/library/venv.html - информация
                             примечание "python3 -m venv /path/to/new/virtual/environment"

- source refueler/bin/activate  запуск виртуального окружения 
                                примечание " source <venv>/bin/activate "

Следующие команды рекомендуется выполнять из виртуального окружения.

ПОСТАВИТЬ ПОСТГРЕСС
brew intall postgresql
psql postgres
create user fuel_db_user with encrypted password 'supersecretpassword';
createdb refueler;
GRANT ALL PRIVILEGES ON DATABASE "refueler" to fuel_db_user;
\q

- python3 -m pip install -r requirements.txt команда для скачивания всех модулей необходимых для работы программы указанных в файле requirements.txt

- для подключения к DB необходимо отредактировать в файле /main.py строку: 
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://fuel_db_user:supersecretpassword@localhost/refueler"
где  "PostgreSQL://username:password@server/db" (тестировалось на PostgreSQL version = 14.2). (https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/ - документация)

- для миграции моделей в DB:
    export FLASK_APP=main.py        #    set FLASK_APP=main.py  - Windows
    flask db init
    flask db migrate
    flask db upgrade

- для корректной работы карты необходимо указать API_key Яндекс карт в 15 строке файла 'main.py'.
Для получения API ключа необходимо авторизоваться на сайте разработчиков Яндекс.Карт. https://developer.tech.yandex.ru/services/
Бесплатная версия имеет ограничение 25 тысяч запросов в сутки. 
Пошаговая инструкция
1. Авторизуйтесь в Яндексе. 
2. Нажмите кнопку Подключить API.
3. Выберите вариант JavaScript API и HTTP Геокодер.
4. Заполните пустые поля: 
— ФИО, почта, телефон
— отметьте, что ваш сервис отвечает условиям бесплатного использования: "в открытой", "в бесплатной", "буду отображать данные на карте".
— добавьте краткое описание сервиса в качестве ссылки на сайт можно указать localhost.
— поставьте галочку в графе Я принимаю условия
5. Нажмите кнопку Продолжить. API подключен.
6. Сервис переведет вас на страницу Кабинета разработчика, где можно скопировать Ключи API.

- в настоящий момент на карте отображается тестовый полигон. Данные о полигоне хранятся в файле '/static/json/geo.json'. 
Для отображения своего полигона необходимо заменить данный файл или указать путь к другому файлу geojson в 16 строке файла 'main.py'.

Для запуска программы на http://127.0.0.1:8000 необходимо выполнить файл main.py